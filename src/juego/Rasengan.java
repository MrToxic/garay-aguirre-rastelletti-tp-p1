package juego;

import java.awt.Color;
import entorno.Entorno;

//#####################################################################################################################################################//

public class Rasengan {
	private double x;
	private double y;
	private double velocidad;
	//private double direccion;
	private int tamaño;
	private Color color;
	
	public Rasengan(double x, double y) {
		this.x = x;
		this.y = y;
		this.velocidad = 3;
		this.tamaño = 20;
		this.color = Color.CYAN;
		//this.direccion = Math.PI/2;
	}
	
//#####################################################################################################################################################//
	
	public void dibujar(Entorno entorno) {
		entorno.dibujarCirculo(x, y, tamaño, color);
	}
	public boolean chocoConBorde(Entorno entorno) {
		return y - tamaño / 2 < 0 || y + tamaño/2>entorno.alto() || x - tamaño / 2 < 0 || x + tamaño/2>entorno.ancho();
	}
	
	public boolean chocasteConNinja(Ninja ninja) {
		return 
				x > ninja.getX() - ninja.getTamaño() / 2
				&&
				x < ninja.getX() + ninja.getTamaño() / 2
				&&
				y - tamaño / 2 < ninja.getY() + ninja.getTamaño() / 2
				&&
				y > ninja.getY() - ninja.getTamaño() / 2;
	}
	
	public void avanzarArriba() {
		y -= velocidad;
	}
	public void avanzarAbajo() {
		y += velocidad;
	}
	public void avanzarDerecha() {
		x += velocidad;
	}
	public void avanzarIzquierda() {
		x -= velocidad;
	}
}
//	public void avanzar() {
//		if(direccion==Math.PI/2) {
//			y-=velocidad;
//		}
//		if(direccion==Math.PI) {
//			x-=velocidad;
//		}
//		if(direccion==Math.PI*1.5) {
//			y+=velocidad;
//		}
//		if(direccion==Math.PI*2) {
//			x+=velocidad;
//		}
//	}
//	public double ApuntaIzquierda() {
//		return direccion=Math.PI;
//	}
//
//	public double ApuntaDerecha() {
//		return direccion = Math.PI*2;
//	}
//
//	public double ApuntaAdelante() {
//		return direccion = Math.PI/2;
//	}
//
//	public double ApuntaAtras() {
//		return direccion = Math.PI*1.5;
//	}
//
//	public double getDireccion() {
//		
//		return direccion;
//	}
