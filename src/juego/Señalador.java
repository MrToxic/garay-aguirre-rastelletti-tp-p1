package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

//#####################################################################################################################################################//

public class Señalador {
	private double x;
	private double y;
	private double tamaño;
	private Image imagen, ramoDeFlores;
	private double angulo;
	private double escala;
	
	Señalador(double x, double y, double tamaño) {
		this.x = x;
		this.y = y;
		this.tamaño = tamaño;
		this.imagen = Herramientas.cargarImagen("imagenes/flecha.png");
		this.ramoDeFlores = Herramientas.cargarImagen("imagenes/ramoDeFlores.png");
		this.angulo = 0;
		this.escala = 0.15;
	}
	
//#####################################################################################################################################################//
	
	public void puntoDeFloreria(Entorno e) {
		e.dibujarImagen(ramoDeFlores, x, y, angulo, 0.5);
	}
	public void puntoDeEntrega0(Entorno e) {
		e.dibujarImagen(imagen, x, y, angulo, escala);
	}
	public void puntoDeEntrega1(Entorno e) {
		e.dibujarImagen(imagen, x, y, angulo, escala);
	}
	public void puntoDeEntrega2(Entorno e) {
		e.dibujarImagen(imagen, x, y, angulo, escala);
	}
	public void puntoDeEntrega3(Entorno e) {
		e.dibujarImagen(imagen, x, y, angulo, escala);
	}
	public void puntoDeEntrega4(Entorno e) {
		e.dibujarImagen(imagen, x, y, angulo, escala);
	}
	
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public double getTamaño() {
		return tamaño;
	}
}
