package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

//#####################################################################################################################################################//

public class Personaje {
	private double x;
	private double y;
	private double tamaño;
	private double velocidad;
	private Image sakuraConRamo, sakuraSinRamo;
	private double angulo;
	private double escala;

	public Personaje(double x, double y, double tamaño, double velocidad) {
		this.x = x;
		this.y = y;
		this.tamaño = tamaño;
		this.velocidad = velocidad;
		this.sakuraConRamo = Herramientas.cargarImagen("imagenes/sakuraConRamo.png");
		this.sakuraSinRamo = Herramientas.cargarImagen("imagenes/sakuraSinRamo.png");
		this.angulo = 0;
		this.escala = 0.5;
	}

//####################################################################################################################################################//
	
	public void dibujarSakuraConRamo(Entorno e) {
		e.dibujarImagen(sakuraConRamo, x, y, angulo, escala);
		System.out.println(x + "/" + y);
	}
	public void dibujarSakuraSinRamo(Entorno e) {
		e.dibujarImagen(sakuraSinRamo, x, y, angulo, escala);
		System.out.println(x + "/" + y);
	}

	public void moverIzquierda() {
		x -= velocidad;
	}

	public void moverDerecha() {
		x += velocidad;
	}

	public void moverAdelante() {
		y -= velocidad;
	}

	public void moverAtras() {
		y += velocidad;
	}

	public boolean chocasteConNinja(Ninja ninja) {
		return ninja.getX() - ninja.getTamaño() / 1.5 < x && x < ninja.getX() + ninja.getTamaño() / 1.5
				&& ninja.getY() - ninja.getTamaño() / 1.5 < y && y < ninja.getY() + ninja.getTamaño() / 1.5;
	}

	public void mantenerseDentroDelEntorno(Entorno entorno) {
		if (chocoLateralIzquierdo()) {
			// Se mantiene sobre el margen izquierdo de la pantalla.
			x = tamaño / 2;
		} else if (chocoLateralDerecho(entorno)) {
			// Se mantiene sobre el margen derecho de la pantalla.
			x = entorno.ancho() - tamaño / 2;
		} else if (chocoBordeSuperior()) {
			// Se mantiene sobre el margen superior de la pantalla.
			y = tamaño / 2;
		} else if (chocoBordeInferior(entorno)) {
			y = entorno.alto() - tamaño / 2;
		}
	}

	public boolean chocoLateralIzquierdo() {
		return x < tamaño / 2;
	}

	public boolean chocoLateralDerecho(Entorno e) {
		return x > e.ancho() - tamaño / 2;
	}

	public boolean chocoBordeSuperior() {
		return y < tamaño / 2;
	}

	public boolean chocoBordeInferior(Entorno e) {
		return y > e.alto() - tamaño / 2;
	}
	
	public boolean chocasteConSeñalador(Señalador s) {
		return s.getX() - s.getTamaño() / 2 < x && x < s.getX() + s.getTamaño() / 2
				&& s.getY() - s.getTamaño() / 2 < y && y < s.getY() + s.getTamaño() / 2;
	}	
	
	
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public double getTamaño() {
		return tamaño;
	}
}