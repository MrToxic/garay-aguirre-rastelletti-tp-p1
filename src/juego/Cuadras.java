package juego;

import java.awt.Color;
import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

//#####################################################################################################################################################//

public class Cuadras {
	private double x;
	private double y;
	private double ancho;
	private double alto;
	private Color color;
	private Image imagen, floreria;
	private double escala;

	public Cuadras(double x, double y, double ancho, double alto, Color color) {
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
		this.color = color;
		this.imagen = Herramientas.cargarImagen("imagenes/casa.jpg");
		this.escala = 1;
		this.floreria = Herramientas.cargarImagen("imagenes/floreria.png");
	}

//#####################################################################################################################################################//
	
	public void dibujar(Entorno entorno) {
		for (double j = y; j < entorno.alto(); j += 65 + entorno.alto() / 6) {
			for (double i = x; i < entorno.ancho(); i += 72 + entorno.ancho() / 8) {
				entorno.dibujarRectangulo(i, j, ancho, alto, 0, color);
				entorno.dibujarImagen(imagen, i, j, 0, escala);
				if(i == 400 && j == 217) {
					entorno.dibujarImagen(floreria, i, j, 0, escala);
				}
			}
		}
	}
	public void dibujarCuadroPuntaje(Entorno entorno) {
		for(double i = 56; i <= 744; i += 688) {
			entorno.dibujarRectangulo(i, 52, ancho, alto, 0, color);
		}
	}
	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getAncho() {
		return ancho;
	}
	public double getAlto() {
		return alto;
	}
}
