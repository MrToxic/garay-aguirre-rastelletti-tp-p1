package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

	private Entorno entorno;
	private Personaje personaje;
	private Rasengan rasengan;
	private Cuadras cuadras;
	private Cuadras cuadroPuntaje;
	private Ninja[] ninjas;
	private Señalador[] señalador;
	private Image portada;
	private int rasenganDisponibles;
	private int direccionDeRasengan;
	private int tiempoEnParalelo;
	private boolean finalizado;
	private boolean instrucciones;
	private boolean inicializado;
	private boolean bloquearInicio;
	private boolean bloqueoDeRasengan;
	private boolean retornarFloreria;
	
	//################################################################################################################################################//	
	//ESTADISTICAS VISIBLES
	
	private int contadorPuntos;
	private int ninjaEliminado;
	private int puntosPorEntrega;
	private int tiempo;

	//################################################################################################################################################//
	// INICIALIZA EL OBJETO ENTORNO
	
	public Juego() {

		setInicializado(true);
		setInstrucciones(false);
		setFinalizado(false);
		bloquearInicio = true;
		bloqueoDeRasengan = false;
		retornarFloreria = false;
		portada = Herramientas.cargarImagen("imagenes/portada.png");
		tiempo = 0;
		tiempoEnParalelo = 0;
		ninjaEliminado = 0;
		contadorPuntos = 0;
		puntosPorEntrega = 0;
		direccionDeRasengan = 0;
		rasenganDisponibles = 2;

		this.entorno = new Entorno(this, "Sakura Ikebana Delivery", 800, 600);
		this.ninjas = new Ninja[4];
		this.señalador = new Señalador[6];

		cuadras = new Cuadras(56, 52, 112, 105, Color.GREEN);
		cuadroPuntaje = new Cuadras(56, 52, 112, 105, Color.GRAY);
		
		personaje = new Personaje(400, 301.5, 60, 2.5);

		ninjas[0] = new Ninja(entorno.ancho() / 5, 134, 60, 2);
		ninjas[1] = new Ninja(entorno.ancho() / 5, 466, 60, 2);
		ninjas[2] = new Ninja(142, entorno.alto() / 6, 60, 2);
		ninjas[3] = new Ninja(658, entorno.alto() / 6, 60, 2);
		
		señalador[0] = new Señalador(142, 52, 20);
		señalador[1] = new Señalador(314, 217, 20);
		señalador[2] = new Señalador(486, 547, 20);
		señalador[3] = new Señalador(744, 135, 20);
		señalador[4] = new Señalador(228, 465, 20);
		señalador[5] = new Señalador(400, 301.5, 20);

		//############################################################################################################################################//
		// INICIA EL JUEGO!
		
		this.entorno.iniciar();
	}

	/**
	 * Durante el juego, el método tick() será ejecutado en cada instante y por lo
	 * tanto es el método más importante de esta clase. Aquí se debe actualizar el
	 * estado interno del juego para simular el paso del tiempo (ver el enunciado
	 * del TP para mayor detalle).
	 */

	//################################################################################################################################################//
	// RELOJ
	
	public void tick() {
		
		//############################################################################################################################################//
		// CONTADOR DE TIEMPO
		
		tiempo += 1;
		tiempoEnParalelo += 1;
		
		//############################################################################################################################################//
		// PANTALLA DE INICIO
		
		if (iniciaste() != false) {
			portada();
			if (entorno.sePresiono(entorno.TECLA_ENTER)) {
					setInicializado(false);
					setInstrucciones(true);
					tiempo = tiempo - tiempoEnParalelo;
			}
		}	
		
		if (instrucciones() != false) {
			instruccionesScreen();
			if (tiempo > 500) {
				bloquearInicio = false;
				setInstrucciones(false);
			}
		}
		//############################################################################################################################################//
		// PANTALLA DE JUEGO
		
		if (perdiste() != true && bloquearInicio != true) {
			
			cuadras.dibujar(entorno);
			cuadroPuntaje.dibujarCuadroPuntaje(entorno);
			if (retornarFloreria == false) {
				personaje.dibujarSakuraConRamo(entorno);
			} else { personaje.dibujarSakuraSinRamo(entorno); }
			mostrarEstadisticasDelJuego();
			
			//########################################################################################################################################//
			// PUNTOS DE ENTREGA
			
			if (señalador[0] != null) {
				señalador[0].puntoDeEntrega0(entorno);
			}
			if (señalador[0] == null && retornarFloreria == true) {
				señalador[5].puntoDeFloreria(entorno);
			}
			if (señalador[1] != null && señalador[0] == null && retornarFloreria == false) {
				señalador[1].puntoDeEntrega1(entorno);
			}
			if (señalador[1] == null && retornarFloreria == true) {
				señalador[5].puntoDeFloreria(entorno);
			}
			if (señalador[2] != null && señalador[1] == null && retornarFloreria == false) {
				señalador[2].puntoDeEntrega2(entorno);
			}
			if (señalador[2] == null && retornarFloreria == true) {
				señalador[5].puntoDeFloreria(entorno);
			}
			if (señalador[3] != null && señalador[2] == null && retornarFloreria == false) {
				señalador[3].puntoDeEntrega3(entorno);
			}
			if (señalador[3] == null && retornarFloreria == true) {
				señalador[5].puntoDeFloreria(entorno);
			}
			if (señalador[4] != null && señalador[3] == null && retornarFloreria == false) {
				señalador[4].puntoDeEntrega4(entorno);
			}
			if (señalador[4] == null && retornarFloreria == true) {
				señalador[5].puntoDeFloreria(entorno);
			}
			if (señalador[4] == null && retornarFloreria == false) {
				setFinalizado(true);
			}
			
			//########################################################################################################################################//
			// NINJAS
			
			//----------------------------------------------------------------------------------------------------------------------------------------//
			// SPAWN
			
			if (ninjas[0] != null) {
				ninjas[0].dibujarNinja0(entorno);
			}if (ninjas[1] != null) {
				ninjas[1].dibujarNinja1(entorno);
			}if (ninjas[2] != null) {
				ninjas[2].dibujarNinja2(entorno);
			}if (ninjas[3] != null) {
				ninjas[3].dibujarNinja3(entorno);
				
			}if (ninjas[0] == null && tiempo % 1000 == 0){
				ninjas[0] = new Ninja(entorno.ancho() / 5, 135, 60, 2);
			}if (ninjas[1] == null && tiempo % 1000 == 0){
				ninjas[1] = new Ninja(entorno.ancho() / 5, 465, 60, 2);
			}if (ninjas[2] == null && tiempo % 1000 == 0){
				ninjas[2] = new Ninja(142, entorno.alto() / 6, 60, 2);
			}if (ninjas[3] == null && tiempo % 1000 == 0){
				ninjas[3] = new Ninja(658, entorno.alto() / 6, 60, 2);
			}
			
			//----------------------------------------------------------------------------------------------------------------------------------------//
			// MOVIMIENTO
			
			if(ninjas[0] != null) {
				ninjas[0].moverDerecha();
			}
			if(ninjas[1] != null) {
				ninjas[1].moverIzquierda();
			}
			if(ninjas[2] != null) {
				ninjas[2].moverArriba();
			}
			if(ninjas[3] != null) {
				ninjas[3].moverAbajo();
			}
			
			//----------------------------------------------------------------------------------------------------------------------------------------//
			// LÍMITES
			
			if (ninjas[0] != null) {
				if (ninjas[0].chocasteConLateralIzquierdo(entorno)) {
					ninjas[0].volverAlPrincipio(entorno);
				}
			}
			if (ninjas[1] != null) {
					if (ninjas[1].chocasteConLateralDerecho()) {
						ninjas[1].volverAlPrincipio(entorno);
					}
			}
			if (ninjas[2] != null) {
					if (ninjas[2].chocasteConBordeSuperior()) {
						ninjas[2].volverAlPrincipio(entorno);
					}
			}
			if (ninjas[3] != null) {
					if (ninjas[3].chocasteConBordeInferior(entorno)) {
						ninjas[3].volverAlPrincipio(entorno);
					}
			}
			
			//########################################################################################################################################//
			// COLISIONES DEL PERSONAJE
			
			//----------------------------------------------------------------------------------------------------------------------------------------//
			// CON NINJAS
			
			for (int i = 0; i < ninjas.length; i++) {
				if (ninjas[i] != null) {
					if (personaje.chocasteConNinja(ninjas[i])) {
						setFinalizado(true);
					}
				}
			}
			
			//----------------------------------------------------------------------------------------------------------------------------------------//
			// CON PUNTOS DE ENTREGA
			
			if (señalador[0] != null && retornarFloreria == false && personaje.chocasteConSeñalador(señalador[0])) {
				contadorPuntos += 5;
				puntosPorEntrega += 1;
				retornarFloreria = true;
				señalador[0] = null;
			}
			if (señalador[1] != null && retornarFloreria == false && señalador[0] == null && personaje.chocasteConSeñalador(señalador[1])) {
				contadorPuntos += 5;
				puntosPorEntrega += 1;
				retornarFloreria = true;
				señalador[1] = null;
			}
			if (señalador[2] != null && retornarFloreria == false && señalador[1] == null && personaje.chocasteConSeñalador(señalador[2])) {
				contadorPuntos += 5;
				puntosPorEntrega += 1;
				retornarFloreria = true;
				señalador[2] = null;
			}
			if (señalador[3] != null && retornarFloreria == false && señalador[2] == null && personaje.chocasteConSeñalador(señalador[3])) {
				contadorPuntos += 5;
				puntosPorEntrega += 1;
				retornarFloreria = true;
				señalador[3] = null;
			}
			if (señalador[4] != null && retornarFloreria == false && señalador[3] == null && personaje.chocasteConSeñalador(señalador[4])) {
				contadorPuntos += 5;
				puntosPorEntrega += 1;
				retornarFloreria = true;
				señalador[4] = null;
			}
			for (int i = 0; i < 5; i++) {
				if (señalador[5] != null && retornarFloreria == true && señalador [i] == null && personaje.chocasteConSeñalador(señalador[5])) {
					contadorPuntos += 0;
					puntosPorEntrega += 0;
					retornarFloreria = false;
				}
			}
			
			//----------------------------------------------------------------------------------------------------------------------------------------//
			// CON BORDES DE PANTALLA
			
			if (personaje.chocoLateralIzquierdo()) {
				personaje.mantenerseDentroDelEntorno(entorno);
			}
			if (personaje.chocoLateralDerecho(entorno)) {
				personaje.mantenerseDentroDelEntorno(entorno);
			}
			if (personaje.chocoBordeInferior(entorno)) {
				personaje.mantenerseDentroDelEntorno(entorno);
			}
			if (personaje.chocoBordeSuperior()) {
				personaje.mantenerseDentroDelEntorno(entorno);
			}
			
			//########################################################################################################################################//
			// RASENGAN
			
			//----------------------------------------------------------------------------------------------------------------------------------------//
			// SPAWN
			
			if (rasengan != null && bloqueoDeRasengan == true) {
				rasengan.dibujar(entorno);
				
				//------------------------------------------------------------------------------------------------------------------------------------//
				// MOVIMIENTO
				
				if (direccionDeRasengan == 0) {
					rasengan.avanzarArriba();
				}
				if (direccionDeRasengan == 1) {
					rasengan.avanzarArriba();
				}
				if (direccionDeRasengan == 2) {
					rasengan.avanzarIzquierda();
				}
				if (direccionDeRasengan == 3) {
					rasengan.avanzarAbajo();
				}
				if (direccionDeRasengan == 4) {
					rasengan.avanzarDerecha();
				}
				
				//------------------------------------------------------------------------------------------------------------------------------------//
				// LÍMITE
				
				if (rasengan.chocoConBorde(entorno)) {
					rasengan = null;
					bloqueoDeRasengan = false;
				}
			}
			
			//----------------------------------------------------------------------------------------------------------------------------------------//
			// AUMENTA EN 1 RASENGAN CADA 10 SEGUNDOS
			
			if (tiempo % 1000 == 0) {
				rasenganDisponibles += 1;
			}
			
			//----------------------------------------------------------------------------------------------------------------------------------------//
			// COLISIÓN CON NINJA
			
			for (int i = 0; i < ninjas.length; i++) {
				if (ninjas[i] != null && rasengan != null && rasengan.chocasteConNinja(ninjas[i])) {
					bloqueoDeRasengan = false;
					rasengan = null;
					ninjas[i] = null;
					contadorPuntos += 2;
					ninjaEliminado+=1;
				}
			}
			
			//########################################################################################################################################//
			// TECLAS DE ACCION
			
			//----------------------------------------------------------------------------------------------------------------------------------------//
			//  DISPARAR RASENGAN
			
			if (rasengan == null && entorno.sePresiono(entorno.TECLA_ESPACIO) && rasenganDisponibles > 0 && bloqueoDeRasengan == false) {
				rasengan = new Rasengan(personaje.getX(), personaje.getY());
				rasenganDisponibles -= 1;
				bloqueoDeRasengan = true;
			}
			
//				if (rasengan == null && entorno.sePresiono(entorno.TECLA_ESPACIO) && rasenganDisponibles > 0) {
//					rasengan = new Rasengan(personaje.getX(), personaje.getY(), 2,rasengan.getDireccion());
//					rasenganDisponibles -= 1;
//				}
//				if(entorno.sePresiono('a')||entorno.sePresiono(entorno.TECLA_IZQUIERDA)) {
//					rasengan.ApuntaIzquierda();
//				}
//				if(entorno.sePresiono('d')||entorno.sePresiono(entorno.TECLA_DERECHA)) {
//					rasengan.ApuntaDerecha();
//				}
//				if(entorno.sePresiono('w')||entorno.sePresiono(entorno.TECLA_ARRIBA)) {
//					rasengan.ApuntaAdelante();
//				}
//				if(entorno.sePresiono('s')||entorno.sePresiono(entorno.TECLA_ABAJO)) {
//					rasengan.ApuntaAtras();
//				}

			//----------------------------------------------------------------------------------------------------------------------------------------//
			// MOVIMIENTO
			
			if (entorno.estaPresionada('a') || entorno.estaPresionada(entorno.TECLA_IZQUIERDA)) {
				if (personaje.getY() >= 130 && personaje.getY() <= 141 || 
					personaje.getY() >= 296 && personaje.getY() <= 307 || 
					personaje.getY() >= 462 && personaje.getY() <= 473) {
					personaje.moverIzquierda();
					if (bloqueoDeRasengan == false) {
						direccionDeRasengan = 2;
					}
				}
			}
			if (entorno.estaPresionada('d') || entorno.estaPresionada(entorno.TECLA_DERECHA)) {
				if (personaje.getY() >= 130 && personaje.getY() <= 141 || 
					personaje.getY() >= 296 && personaje.getY() <= 307 || 
					personaje.getY() >= 462 && personaje.getY() <= 473) {
					personaje.moverDerecha();
					if (bloqueoDeRasengan == false) {
						direccionDeRasengan = 4;
					}
				}
			}
			if (entorno.estaPresionada('w') || entorno.estaPresionada(entorno.TECLA_ARRIBA)) {
				if (personaje.getX() >= 137 && personaje.getX() <= 147 || 
					personaje.getX() >= 309 && personaje.getX() <= 319 || 
					personaje.getX() >= 481 && personaje.getX() <= 491 ||
					personaje.getX() >= 653 && personaje.getX() <= 663) {
					personaje.moverAdelante();
					if (bloqueoDeRasengan == false) {
						direccionDeRasengan = 1;
					}
				}
			}
			if (entorno.estaPresionada('s') || entorno.estaPresionada(entorno.TECLA_ABAJO)) {
				if (personaje.getX() >= 137 && personaje.getX() <= 147 || 
					personaje.getX() >= 309 && personaje.getX() <= 319 || 
					personaje.getX() >= 481 && personaje.getX() <= 491 ||
					personaje.getX() >= 653 && personaje.getX() <= 663) {
					personaje.moverAtras();
					if (bloqueoDeRasengan == false) {
						direccionDeRasengan = 3;
					}
				}
			}
			
		//############################################################################################################################################//
		// PANTALLA FINAL
			
		} else if (bloquearInicio != true) {
			gameOver();
			if (entorno.sePresiono(entorno.TECLA_ENTER)) {
				juegoNuevo();
			}
		}
	}
	
	//################################################################################################################################################//
	// JUEGO NUEVO
	
	private void juegoNuevo() {
		
		setFinalizado(false);
		tiempo = 0;
		ninjaEliminado= 0;
		contadorPuntos = 0;
		puntosPorEntrega = 0;
		rasenganDisponibles = 2;
		retornarFloreria = false;

		this.entorno = new Entorno(this, "Sakura Ikebana Delivery", 800, 600);
		this.ninjas = new Ninja[4];
		this.señalador = new Señalador[6];

		cuadras = new Cuadras(56, 52, 112, 105, Color.GREEN);
		cuadroPuntaje = new Cuadras(56, 52, 112, 105, Color.GRAY);
		
		personaje = new Personaje(entorno.ancho() / 2, 301.5, 60, 1.5);

		ninjas[0] = new Ninja(entorno.ancho() / 5, 135, 55, 1);
		ninjas[1] = new Ninja(entorno.ancho() / 5, 465, 55, 1);
		ninjas[2] = new Ninja(142, entorno.alto() / 6, 55, 1);
		ninjas[3] = new Ninja(658, entorno.alto() / 6, 55, 1);
		
		señalador[0] = new Señalador(142, 52, 20);
		señalador[1] = new Señalador(314, 217, 20);
		señalador[2] = new Señalador(486, 547, 20);
		señalador[3] = new Señalador(744, 135, 20);
		señalador[4] = new Señalador(228, 465, 20);
		señalador[5] = new Señalador(400, 301.5, 20);

		// Inicia el juego!
		this.entorno.iniciar();
		
	}
	
	//################################################################################################################################################//
	// TEXTOS
	
	public void portada() {
		entorno.dibujarRectangulo(0, 0, entorno.ancho()*2, entorno.alto()*2, 0, Color.PINK);
		entorno.dibujarImagen(portada, entorno.ancho() / 2, entorno.alto() / 2, 0, 1.15);
	}
	public void instruccionesScreen() {
		entorno.dibujarRectangulo(0, 0, entorno.ancho()*2, entorno.alto()*2, 0, Color.GRAY);
		entorno.dibujarRectangulo(400, 300, 700, 500, 0, Color.PINK);
		entorno.cambiarFont("Arial", 30, Color.GRAY);
		entorno.escribirTexto("Distribuye todos los ramos de flores por la ciudad.", 70, 200);
		entorno.escribirTexto("¡Cuidado con los ninjas!", 250, 450);
	}

	// Funcion que muestra texto y puntos en pantalla cuando se pierde en el juego
	public void gameOver() {
		entorno.cambiarFont("Arial", 45, Color.WHITE);
		entorno.escribirTexto("Juego Terminado", entorno.ancho() / 4+30, entorno.alto() / 2 + 15);
		entorno.cambiarFont("Arial", 30, Color.WHITE);
		entorno.escribirTexto("Puntos: " + Integer.toString(contadorPuntos), entorno.ancho() * 0.1, entorno.alto() - 150);
		entorno.escribirTexto("Presione ''Enter'' para jugar nuevamente", entorno.ancho() * 0.1, entorno.alto() * 0.9);

	}
	public void mostrarEstadisticasDelJuego() {
		entorno.cambiarFont("Arial", 16, Color.RED);
		entorno.escribirTexto("Tiempo: " + Integer.toString(tiempo/100), 16, 30);
		entorno.cambiarFont("Arial", 16, Color.WHITE);
		entorno.escribirTexto("Puntos: " + Integer.toString(contadorPuntos), entorno.ancho()/1.15, 30);
		entorno.escribirTexto("Entregas: " + Integer.toString(puntosPorEntrega), entorno.ancho()/1.15, 90);
		entorno.escribirTexto("Ninjas: " + Integer.toString(ninjaEliminado), 16, 60);
		entorno.escribirTexto("Rasengan: " + Integer.toString(rasenganDisponibles) , 16, 90);
	}

	//################################################################################################################################################//
	
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}

	public boolean perdiste() {
		return finalizado;
	}

	public void setFinalizado(boolean finalizado) {
		this.finalizado = finalizado;
	}
	
	public boolean iniciaste() {
		return inicializado;
	}

	public void setInicializado(boolean inicializado) {
		this.inicializado = inicializado;
	}
	
	public boolean instrucciones() {
		return instrucciones;
	}
	
	private void setInstrucciones(boolean instrucciones) {
		this.instrucciones = instrucciones;
		
	}
}
