package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

//####################################################################################################################################################//

public class Ninja {
	private double x;
	private double y;
	private double tamaño;
	private double velocidad;
	private Image imgNinja0, imgNinja1, imgNinja2, imgNinja3;
	private double angulo;
	private double escala;

	public Ninja(double x, double y, double tamaño, double velocidad) {
		this.x = x;
		this.y = y;
		this.tamaño = tamaño;
		this.velocidad = velocidad;
		this.imgNinja0 = Herramientas.cargarImagen("imagenes/ninja1.png");
		this.imgNinja1 = Herramientas.cargarImagen("imagenes/ninja2.png");
		this.imgNinja2 = Herramientas.cargarImagen("imagenes/ninja3.png");
		this.imgNinja3 = Herramientas.cargarImagen("imagenes/ninja4.png");
		this.angulo = 0;
		this.escala = 0.5;
	}
	
//####################################################################################################################################################//

	public void dibujarNinja0(Entorno e) {
		e.dibujarImagen(imgNinja0, x, y, angulo, escala);
	}
	public void dibujarNinja1(Entorno e) {
		e.dibujarImagen(imgNinja1, x, y, angulo, escala);
	}
	public void dibujarNinja2(Entorno e) {
		e.dibujarImagen(imgNinja2, x, y, angulo, escala);
	}
	public void dibujarNinja3(Entorno e) {
		e.dibujarImagen(imgNinja3, x, y, angulo, escala);
	}

	public void moverIzquierda() {
		x -= velocidad;
	}

	public void moverDerecha() {
		x += velocidad;
	}

	public void moverArriba() {
		y -= velocidad;
	}

	public void moverAbajo() {
		y += velocidad;
	}

	public boolean chocasteConLateralIzquierdo(Entorno e) {
		return x > e.ancho() + tamaño / 2;
	}

	public boolean chocasteConLateralDerecho() {
		return x < -tamaño / 2;
	}

	public boolean chocasteConBordeSuperior() {
		return y < -tamaño / 2;
	}

	public boolean chocasteConBordeInferior(Entorno e) {
		return y > e.alto() + tamaño / 2;
	}

	public void volverAlPrincipio(Entorno e) {
		if (chocasteConLateralIzquierdo(e)) {
			x = 0 - tamaño / 2;
		} else if (chocasteConLateralDerecho()) {
			x = e.ancho() + tamaño / 2;
		} else if (chocasteConBordeSuperior()) {
			y = e.alto() + tamaño / 2;
		} else if (chocasteConBordeInferior(e)) {
			y = 0 - tamaño / 2;
		}

	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getTamaño() {
		return tamaño;
	}
}
